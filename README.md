# [https://streaming-ondemand.xyz](https://streaming-ondemand.xyz)


<div align="center">
  <img src="logo.png">
  <p>
    Dashy is the <a href="https://gitlab.com/gonzaloberteri/streaming-ondemand.xyz">streaming-ondemand</a> service in charge of generating the necesary files and uploading to S3
  </p>
</div>

## Setup

```bash
npm i
npm start
```
## Tests

```bash
npm test
```

## About dashy
Under the hood dashy uses [FFMPEG](https://ffmpeg.org/) and [Shaka packager](https://github.com/google/shaka-packager) to generate:
- MPD manifest 
- 720p video file
- 360p video file
- audio track
- HD screenshot
- preview GIF

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
