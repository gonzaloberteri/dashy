const fs = require("fs");

module.exports = {
    key: fs.readFileSync("./certificates/localhost+2-key.pem"),
    cert: fs.readFileSync("./certificates/localhost+2.pem"),
    requestCert: false,
    rejectUnauthorized: false,
};
