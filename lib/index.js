var app = require("express")();
const fileUpload = require("express-fileupload");
const https = require("https");
const httpsOptions = require("./config/httpsOptions");

app.enable("trust proxy");
app.use(fileUpload());

require("./config/db");

app.use(require("./routes/api/uploadMovie"));

https.createServer(httpsOptions, app).listen(3000, () => {
    console.log("________              ______          ");
    console.log("___  __ \\_____ __________  /______  __");
    console.log("__  / / /  __ `/_  ___/_  __ \\_  / / /");
    console.log("_  /_/ // /_/ /_(__  )_  / / /  /_/ / ");
    console.log("/_____/ \\__,_/ /____/ /_/ /_/_\\__, /  ");
    console.log("                             /____/   ");
    console.log("listening on http://localhost:3000");
});
