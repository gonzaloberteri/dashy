#!/bin/bash
#usage: ./run.sh <input_file> <output_name>

ffmpeg='./convert/bin/ffmpeg-4.3.1-amd64-static/ffmpeg'
packager='./convert/bin/packager-linux'

mkdir convert/output/mp4/$2
mkdir convert/output/mpd/$2
#720p encoding
$ffmpeg -y -i convert/input/$1 -c:a aac -ac 2 -ab 256k -ar 48000 -c:v libx264 -x264opts 'keyint=24:min-keyint=24:no-scenecut' -b:v 1500k -maxrate 1500k -bufsize 1000k -vf "scale=-1:720" convert/output/mp4/$2/$2-720.mp4
#360p encoding 
$ffmpeg -y -i convert/input/$1 -c:a aac -ac 2 -ab 64k -ar 22050 -c:v libx264 -x264opts 'keyint=24:min-keyint=24:no-scenecut' -b:v 400k -maxrate 400k -bufsize 400k -vf "scale=-1:360" convert/output/mp4/$2/$2-360.mp4

#hq screenshot
$ffmpeg -ss 00:00:5 -i convert/input/$1 -vframes 1 -q:v 2 convert/output/mpd/$2/$2-hq.jpg

#preview image sequence
$ffmpeg -i convert/input/$1 -vf fps=1 convert/output/mpd/$2/$2-%04d.png

#image sequence to video
$ffmpeg -f image2 -i convert/output/mpd/$2/$2-%04d.png convert/output/mpd/$2/video.avi

#video to gif
$ffmpeg -i convert/output/mpd/$2/video.avi -pix_fmt rgb24 convert/output/mpd/$2/$2-preview.gif

#shaka packager
$packager \
in=convert/output/mp4/$2/$2-360.mp4,stream=audio,output=convert/output/mpd/$2/$2-audio.mp4 \
in=convert/output/mp4/$2/$2-360.mp4,stream=video,output=convert/output/mpd/$2/$2-360p.mp4 \
in=convert/output/mp4/$2/$2-720.mp4,stream=video,output=convert/output/mpd/$2/$2-720p.mp4 \
--mpd_output convert/output/mpd/$2/$2.mpd